//
//  ACRaterView.swift
//  Arun Chandran
//
//  Created by Arun Chandran on 30/10/16.
//  Copyright © 2016 Arun Chandran. All rights reserved.
//

import UIKit

protocol ACRaterViewDelegate: NSObjectProtocol{
    func ratingSelected()
}

class ACRaterView: UIView {
    @IBOutlet var raterContentView:UIView!
    @IBOutlet var raterStar1: UIButton!
    @IBOutlet var raterStar2: UIButton!
    @IBOutlet var raterStar3: UIButton!
    @IBOutlet var raterStar4: UIButton!
    @IBOutlet var raterStar5: UIButton!
    
    weak var delegate: ACRaterViewDelegate!
    
    var productRating: Int = 0
    
    enum rateSelectionType {
        case manual
        case auto
    }
    
    var selectionType:rateSelectionType!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        NSBundle.mainBundle().loadNibNamed("ACRaterView", owner: self, options: nil)
        self.addSubview(self.raterContentView)
        self.raterContentView.frame = self.bounds
    }
    
    func setRating(rating:Int) -> Void
    {
        productRating = rating
        print("rating value is \(rating)")
        if(rating == 0){
            self.makeStarDisabled(raterStar1)
            self.makeStarDisabled(raterStar2)
            self.makeStarDisabled(raterStar3)
            self.makeStarDisabled(raterStar4)
            self.makeStarDisabled(raterStar5)
        }
        else if(rating == 1){
            self.makeStarEnabled(raterStar1)
            self.makeStarDisabled(raterStar2)
            self.makeStarDisabled(raterStar3)
            self.makeStarDisabled(raterStar4)
            self.makeStarDisabled(raterStar5)
            self.animateButton(raterStar1)
        }
        else if(rating == 2){
            self.makeStarEnabled(raterStar1)
            self.makeStarEnabled(raterStar2)
            self.makeStarDisabled(raterStar3)
            self.makeStarDisabled(raterStar4)
            self.makeStarDisabled(raterStar5)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar1, afterDelay: 0.1)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar2, afterDelay: 0.16)
        }
        else if(rating == 3){
            self.makeStarEnabled(raterStar1)
            self.makeStarEnabled(raterStar2)
            self.makeStarEnabled(raterStar3)
            self.makeStarDisabled(raterStar4)
            self.makeStarDisabled(raterStar5)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar1, afterDelay: 0.1)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar2, afterDelay: 0.16)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar3, afterDelay: 0.22)
        }
        else if(rating == 4){
            self.makeStarEnabled(raterStar1)
            self.makeStarEnabled(raterStar2)
            self.makeStarEnabled(raterStar3)
            self.makeStarEnabled(raterStar4)
            self.makeStarDisabled(raterStar5)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar1, afterDelay: 0.1)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar2, afterDelay: 0.16)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar3, afterDelay: 0.22)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar4, afterDelay: 0.28)
        }
        else if(rating == 5){
            self.makeStarEnabled(raterStar1)
            self.makeStarEnabled(raterStar2)
            self.makeStarEnabled(raterStar3)
            self.makeStarEnabled(raterStar4)
            self.makeStarEnabled(raterStar5)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar1, afterDelay: 0.1)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar2, afterDelay: 0.16)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar3, afterDelay: 0.22)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar4, afterDelay: 0.28)
            self.performSelector(#selector(ACRaterView.animateButton(_:)), withObject: raterStar5, afterDelay: 0.34)
        }
       delegate?.ratingSelected()
    }
    
    func getRating() -> Int
    {
        return productRating
    }
    
    @IBAction func ratingStarTapped(sender: AnyObject!){
        self.setRating(sender.tag)
    }
    
    func makeStarEnabled(button: UIButton) -> Void
    {
        button.setImage(UIImage(named: "raterStarSelected"), forState: UIControlState.Normal)
        button.setImage(UIImage(named: "raterStarSelected"), forState: UIControlState.Highlighted)
    }
    
    func makeStarDisabled(button: UIButton) -> Void
    {
        button.setImage(UIImage(named: "raterStarUnselected"), forState: UIControlState.Normal)
        button.setImage(UIImage(named: "raterStarUnselected"), forState: UIControlState.Highlighted)
    }
    
    func animateButton(button: UIButton) -> Void{
        
        UIView.animateWithDuration(0.1, delay: 0.0, options:UIViewAnimationOptions.CurveEaseOut, animations: {() -> Void in
            button.transform = CGAffineTransformMakeScale(1.3, 1.3)
            
            }, completion:{
                (Bool) -> Void in
                UIView.animateWithDuration(0.1, delay: 0.0, options:UIViewAnimationOptions.CurveEaseIn, animations: {() -> Void in
                    button.transform = CGAffineTransformMakeScale(1.0, 1.0)
                    
                    }, completion:{
                        (Bool) -> Void in
                        
                }   )
        }   )
    }
}

