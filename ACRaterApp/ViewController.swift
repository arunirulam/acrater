//
//  ViewController.swift
//  ACRaterApp
//
//  Created by Arun Chandran on 10/30/16.
//  Copyright © 2016 aruns. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ACRaterViewDelegate {
    
    @IBOutlet weak var raterView: ACRaterView!
    @IBOutlet weak var ratingLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        raterView.delegate = self
        self.ratingLabel.text = "Arun"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func ratingSelected(){
        self.ratingLabel.text = "Rating is \(raterView.getRating())"
        print("Rating is \(raterView.getRating())")
    }
}

